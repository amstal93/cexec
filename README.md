cexec
=====

[![pipeline status](https://gitlab.com/wt0f/docker_executor/badges/master/pipeline.svg)](https://gitlab.com/wt0f/docker_executor/-/commits/master)

Execute Docker containers as regular commands with mount points, env vars and port forwarding without having to remember all the settings.

Configuration file
------------------

Each execution group has a configuration file located in `~/.cexec/etc`
that contains all the necessary settings for starting the container image
when called as a command. The schema for the configuration file is as
follows:

```yaml
---
name: empire
executables:
  - empire
  - pei
  - pei2
container:
  image: registry.gitlab.com/wt0f/empire_clients/empire
  tag: latest
env:
  FOO: bar
volumes:
  /empire: $HOME/games
```

Attribute definitions:

- `name`: This is the name of the execution group and is used with the
        `cexec` command to make changes to the configuration.

- `executables`: This is a list of executable names that can be used to
        start the container. Each of the listed names will receive an
        entry in `~/.cexec/bin` so that the shell will see the executable
        as a command that can be called.

- `container`: This is the container that is started whenever any of the
        listed executables are called as commands. The `tag` attribute
        can be overridden with the `---tag` command line switch. This
        allows one to call multiple versions of a container image without
        having to constantly rewrite a configuration file. This can be
        especially useful for different versions of compilers or scripting
        languages.

- `env`: A list of environmental variables and values to assign for the
        execution of the container image. Note that these variables only
        exist during the execution of the container image.

- `volumes`: A list of file paths that need to be mounted into the
        executing image. The key is the file path within the container
        execution environment and the value is the file path to be mounted
        in the container. The value portion can contain `$PWD`, `$HOME`
        and `~` and will be expanded accordingly.
