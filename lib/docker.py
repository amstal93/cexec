
import os
import subprocess
import re

def check_docker_image(image, tag='latest'):
    proc = subprocess.Popen(['docker', 'images'], stdout=subprocess.PIPE)
    stdout = proc.communicate()

    for line in stdout[0].decode("utf-8").split("\n"):
        if re.match(r'%s\s+%s\b' % (image, tag), line):
            return True
    return False

def download_docker_image(image, tag='latest'):
    proc = subprocess.Popen(['docker', 'pull', '%s:%s' % (image, tag)])
    proc.communicate()

def volume_expansion(vol_str):
    vol_str = os.path.expanduser(vol_str)
    vol_str = os.path.expandvars(vol_str)
    # vol_str = re.sub(r"\$\{?PWD\}?", os.getcwd(), vol_str)
    # vol_str = re.sub(r"\$\{?HOME\}?", os.environ['HOME'], vol_str)
    # vol_str = re.sub(r"~", os.environ['HOME'], vol_str)
    return vol_str

def execute(prog, config, args):
    volumes = ['%s:%s' % (config['volumes'][dir], dir) for dir in config['volumes']]
    env_vars = ['%s=%s' % (var, config['env'][var]) for var in config['env']]

    volume_args = []
    for vol in volumes:
        volume_args.append('-v')
        volume_args.append(volume_expansion(vol))

    env_args = []
    for var in env_vars:
        env_args.append('-e')
        env_args.append(var)

    if config['exec_options']['verbose']:
        print(' '.join(['docker', 'run', '-it', '--rm'] + env_args + volume_args +
            ['%s:%s' % (config['container']['image'], config['container']['tag']),
             prog] + args))
    proc = subprocess.Popen(['docker', 'run', '-it', '--rm'] + env_args + volume_args +
            ['%s:%s' % (config['container']['image'], config['container']['tag']),
             prog] + args)
    proc.communicate()